import os 
import re
import glob
import struct
import os.path
import subprocess

try:
	import lz4
	import lz4.block
	import docopt 
except ImportError: 
	print("Install python-lz4, docopt")
	os._exit(1)


__version__ = "0.1"
__doc__ = r"""
Usage:
	sfc.py <file> [<output>] [options]

Options:
	--convert-dds 	Convert dds to png on the fly ( Needs ffmpeg )
	--help
""".expandtabs(4)


if __name__ == "__main__":
	
	Options = { re.sub( "[\<\>]|^--|^-", "", Key ).replace( "-", "_" ): Value for Key, Value in docopt.docopt( __doc__, version=__version__ ).items() }
	
	for File in glob.glob( Options["file"] ):

		print( f'Decompressing {os.path.basename(File)}' )

		with open( File, "rb" ) as Input:

			if Input.read( 8 ) != b"\x53\x46\x43\x1A\x02\x00\x00\x00":
				print( f"Invalid magic for {os.path.basename(File)}" )
				continue 

			while True:
			
				GUID = Input.read(8).hex()
				try:
					Size = int( struct.unpack( "<I", Input.read(4) )[0] )
				except struct.error:
					break

				if Size <= 0:
					print( f"Invalid size, skipping" ) #TODO
					break

				print( f"	Decompressing {GUID} {Size} bytes" )				

				Pos = Input.tell()
				Input.seek( Size, 1 )
				UncompressedSize = int( struct.unpack( "<I", Input.read(4) )[0] )
				Input.seek( Pos )

				Payload = lz4.block.decompress( Input.read( Size ), uncompressed_size=UncompressedSize )

				if Payload.startswith( b"\x3c\x3f\x78\x6d\x6c\x20" ):
					FileType = "xml"
				elif Payload.startswith( b"DDS"):
					FileType = "dds"
				elif Payload.startswith( b"7z" ):
					FileType = "7z"
				elif Payload.startswith( b"\x89\x50\x4E\x47" ):
					FileType = "png"
				elif Payload.startswith( b"8BPS" ):
					FileType = "psd"
				elif Payload.startswith( b"BKHD" ):
					FileType = "bnk"
				elif Payload.startswith( b"RIFF" ): # Could also be avi, but i doubt they would put that in a game
					FileType = "wav"
				elif Payload.startswith( b"GFX\n"):	
					FileType = "gfx"	
				elif Payload.startswith( b"<!DOCTYPE"):
					FileType = "xml"
				elif re.match( b"^\s?\<\w+ ", Payload ):
					FileType = "xml"		
				elif b"local " in Payload and b" end" in Payload: #TODO
					FileType = "lua"					
				elif Payload.startswith( b"\x46\x57\x53" ) or Payload.startswith( b"\x43\x57\x53" ):
					FileType = "swf"
				else:
					FileType = "bin"

				OutputFile = f"{GUID}.{FileType}" 

				if Options["output"]:
					OutputDir = Options["output"] + "/" + os.path.basename(File)[:-4] + "/"
				else:
					OutputDir = os.path.basename(File)[:-4] + "/"

				print( f"		UncompressedSize:{UncompressedSize} FileType: {FileType}")
				
				if not os.path.exists( OutputDir ):
					os.mkdir( OutputDir )

				with open( OutputDir + OutputFile, "wb" ) as Output:
					Output.write( Payload )

				del Payload

				if FileType == "dds" and Options["convert_dds"]:
					subprocess.check_call( f'ffmpeg -i "{OutputFile}" "{OutputFile}.png" -y', stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL )

				Input.seek(4,1)

		print()
