# sfc

SFC file structure:
```
SFC:
    FileHeader  8 bytes
    Entry
    Entry
    Entry
    ...

FileHeader:
    MAGIC       3 bytes     ="SFC"
    Version?    1 byte      =0x1A
    Version?    4 bytes     =2

Entry:
    GUID?       8 bytes
    Size        4 bytes     Data size
    Data        Size bytes  lz4 block format
    UncSize     4 bytes     Uncompressed data size
```
Thanks to HarrisonFiat for reversing the file format


```
C:\Users\user\Desktop>sfc.py --help
Usage:
    sfc.py <file> [<output>] [options]

Options:
    --convert-dds   Convert dds to png on the fly ( Needs ffmpeg )
    --help
C:\Users\user\Desktop>sfc.py C:\Garena\Games\32839\SFC\*.sfc unpacked_roe
Decompressing Actors_AIRDROP.sfc
        Decompressing ab35372b43c4162f 3937 bytes
                UncompressedSize:14384 FileType: bin
        Decompressing aa55372b43c4162f 25306 bytes
                UncompressedSize:35184 FileType: bin
```